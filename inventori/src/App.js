import Sidebar from "./components/Sidebar";

function App() {
  return (
    <div className="w-full min-h-screen bg-BgF flex flex-row">
      <Sidebar />
      <section className="flex-1 bg-WhiteF h-[60px] text-right p-[18px] shadow-[0px_4px_10px_rgba(0,0,0,0.05)]">Fikri Haykal</section>
    </div>
    
  );
}

export default App;
