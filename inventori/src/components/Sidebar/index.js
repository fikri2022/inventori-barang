import React from 'react'
import Logo from '../../assets/logo.png';
import { MdDashboard, MdEventAvailable } from "react-icons/md";
import { TbArrowsCross } from "react-icons/tb";
import { VscChecklist } from "react-icons/vsc";
import { AiFillFile } from "react-icons/ai";

export default function Sidebar() {
    const menu =[
        {name:"Dashboard", icon:<MdDashboard />},
        {name:"Barang Tersedia", icon:<MdEventAvailable />},
        {name:"Barang Rusak", icon:<TbArrowsCross />},
        {name:"Kategori Barang", icon:<VscChecklist />},
        {name:"Laporan", icon:<AiFillFile />},
    ];

    return <div className='h-screen bg-BlueF w-[214px] shadow-[4px_0px_20px_rgba(0,0,0,0.05)] px-9'>
        <div className='flex flex-row items-center mb-[92px]'>
            <img src={Logo} alt="logoinventori" className='w-[30px] h-[30px] mt-[15px] mr-[8px] ' />
            <div className='text-WhiteF text-[28px] mt-[16px]'>Inventori</div>
        </div>
        <div>
            <ul>
                <div className='mb-4'></div>
                {menu.map((val, index) => {
                        return <li key={index} className='mb-7 flex flex-row items-center'>
                            <div className='mr-[8px] text-WhiteF w-[20px]'>{val.icon}</div>
                            <div className='text-WhiteF text-[16px]'>{val.name}</div>
                        </li>
                })}
            </ul>
        </div>
    </div>
    

}
